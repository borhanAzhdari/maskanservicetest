﻿using MehrHousingService.DTOs;
using MehrHousingService.Extension;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace MehrHousingService.Controllers
{

    //[Route("Api/v1/{Controller}")]
    public class RegisterMehrController : ApiController
    {
        private new const string Url = "https://www.bank-maskan.ir/api/jsonws/loan-request-portlet.mehrplanintroduceletter/register";

        // POST: User Register For Mehr Housing Service
        [HttpPost]
        [BasicAuthentication]
        
        public async Task<IHttpActionResult> RegisterMehr(UserForRegisterMehrDtos user)
        {
            
            user.requestDateString = DateTime.Now.ToShamsi();
            HttpClient http = new HttpClient();
            var userJson = JsonConvert.SerializeObject(user);
            var content = new StringContent(userJson, Encoding.UTF8, "application/json");
            var request = await http.PostAsync(Url, content);
            var resp = await request.Content.ReadAsStringAsync();
            return Ok(resp);
        }

    }
}