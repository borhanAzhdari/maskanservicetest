﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MehrHousingService.DTOs
{
    public class ResponseDTOs
    {
        public string Status { get; set; }
        public DateTime transactionDate { get; set; }
        public long transactionId { get; set; }
    }
}