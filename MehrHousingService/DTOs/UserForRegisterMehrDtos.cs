﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MehrHousingService.DTOs
{
    public class UserForRegisterMehrDtos
    {
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نام")]
        public string firstname { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نام خانوادگی")]
        public string lastname { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نام پدر")]
        public string fatherName { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("شماره شناسنامه")]
        public string certificateNumber { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد ملی")]
        public string nationalCode { get; set; }

        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName(" شماره موبایل")]
        public string mobileNumber { get; set; }

        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نام استان")]
        public string provinceName { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد شهر")]
        public long cityId { get; set; }
       
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد رمز")]
        public string password { get; set; }
        
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد شعبه")]
        public int branchCode { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("شماره پرونده")]
        public string fileNumber { get; set; }
       
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد پروژه")]
        public string projectCode { get; set; }
        
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد بلوک")]
        public string blocksCode { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نام بلوک")]
        public string blocksName { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نوع واحد")]
        public int unitType { get; set; }
      
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("کد واحد")]
        public string unitCode { get; set; }
       
        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("متراژ")]
        public int unitArea { get; set; }

        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("نوع درخواست")]
        public int requestFor { get; set; }

        [Required(ErrorMessage = "فیلد {0}اجباری است"), DisplayName("تاریخ درخواست")]
        public string requestDateString { get; set; }
    }
}